# Hyperstacks, Inc. (Exams)

![Hyperstacks, Inc. logo](https://i.imgur.com/T3TZaaps.jpg)

This repository compiles all the hyperstacks' exams for different positions. Feel free to make any improvements or add another exams.

## Positions (WIP)

List of all possible positions:

* [Frontend](https://gitlab.com/jannomeister/hyperstacks-exams/-/tree/master/frontend)
* [Backend](https://gitlab.com/jannomeister/hyperstacks-exams/-/tree/master/backend)
* [Fullstack](https://gitlab.com/jannomeister/hyperstacks-exams/-/tree/master/fullstack)

## Contributing
Pull requests are welcome. For adding new exams, Please open an issue first to discuss about the exam you like to add.

Please make sure to scan the repository first to avoid duplication and wasting your time.