# Pkg Drag-n-drop

This exam will test your analytical skills and frontend skills


## Instructions
  > **NOTE**: You don't need to copy the designs of the provided samples, just make it as your guide. Feel free to choose any color, fonts, SASS/CSS frameworks, etc... You just need to make it work! :)

Create a [javascript](https://www.w3schools.com/whatis/whatis_js.asp) web application that reads JSON data from a JSON file dragged into the webpage. The app then reads that json data and shows the contents in a [HTML table](https://www.w3schools.com/html/html_tables.asp) below the [drag and drop box](https://www.smashingmagazine.com/2018/01/drag-drop-file-uploader-vanilla-js/).

Your datatable fields are:

* firstName
* lastName
* emailAddress
* age

## Materials to use

* Here is the link of the JSON file: [click & download here](https://gitlab.com/jannomeister/hyperstacks-exams/-/blob/master/frontend/exam_1/data.json)
* Here is the UI design for the drag and drop box: [click here](https://i.imgur.com/46tDCbC.png)
* Here is the UI design for the datatable: [click here](https://i.imgur.com/jnsd4VT.png)

## How to submit your work

* Creat a repo to any git hosting provider of your choice (github, gitlab, bitbucket).
* Don't forget to make it public so that examiners can access it.
* You can notify the examiner and ask for his/her email address and send the git repo link via email.

## Questions

If you have any questions, feel free to ask the HR manager or the Team Lead/Examiner.

&nbsp;